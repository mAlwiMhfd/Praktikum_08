
<head>
<link href="bootstrap.min.css" rel="stylesheet">
</head>

<body>

<form class="form-horizontal" method = "POST">
<fieldset>

<!-- Form Name -->
<legend>Form Nilai Siswa</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Nama">Nama Lengkap</label>
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Nama Lengkap" class="form-control input-md">

  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Nama">NIM</label>
  <div class="col-md-4">
  <input id="nim" name="nim" type="text" placeholder="nim" class="form-control input-md">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="matkul">Mata Kuliah</label>
  <div class="col-md-4">
    <select id="matakuliah" name="matakuliah" class="form-control">
      <option value="1">Dasar - Dasar Pemrograman</option>
      <option value="2">Pemrograman Web</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="uts">Nilai Uts</label>
  <div class="col-md-4">
  <input id="uts" name="uts" type="text" placeholder="NIlai Uts" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Uas">NIlai Uas</label>
  <div class="col-md-4">
  <input id="uas" name="uas" type="text" placeholder="NIlai Uas" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tugas">Nilai Tugas/Praktikum</label>
  <div class="col-md-4">
  <input id="tugas" name="tugas" type="text" placeholder="Nilai Tugas" class="form-control input-md" required="">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button"></label>
  <div class="col-md-4">
   <button id="button" name="button" class="btn btn-primary">Simpan</button>
  </div>
</div>

</fieldset>
</form>
<?php
	
	$ns1 = ['id' => 1, 'nim'=> '01101', 'uts'=>80, 'uas'=>84, 'tugas'=>78];
	$ns2 = ['id' => 2, 'nim'=> '01121', 'uts'=>70, 'uas'=>50, 'tugas'=>68];
	$ns3 = ['id' => 3, 'nim'=> '01130', 'uts'=>60, 'uas'=>86, 'tugas'=>70];
	$ns4 = ['id' => 4, 'nim'=> '01134', 'uts'=>90, 'uas'=>91, 'tugas'=>82];
	$ns5 = ['id' => 5, 'nim'=>$_POST['nim'], 'uts'=> $_POST['uts'], 'uas'=> $_POST['uas'],	'tugas'=> $_POST['tugas']];
	$ns6 = ['id' => 6, 'nim'=>$_POST['nim'], 'uts'=> $_POST['uts'], 'uas'=> $_POST['uas'],	'tugas'=> $_POST['tugas']];
	$ns7 = ['id' => 7, 'nim'=>$_POST['nim'], 'uts'=> $_POST['uts'], 'uas'=> $_POST['uas'],	'tugas'=> $_POST['tugas']];
	$ns8 = ['id' => 8, 'nim'=>$_POST['nim'], 'uts'=> $_POST['uts'], 'uas'=> $_POST['uas'],	'tugas'=> $_POST['tugas']];
	$ns9 = ['id' => 9, 'nim'=>$_POST['nim'], 'uts'=> $_POST['uts'], 'uas'=> $_POST['uas'],	'tugas'=> $_POST['tugas']];
	$ns10 = ['id' => 10, 'nim'=>$_POST['nim'], 'uts'=> $_POST['uts'], 'uas'=> $_POST['uas'],	'tugas'=> $_POST['tugas']];
	
	$ar_nilai = [$ns1, $ns2, $ns3, $ns4, $ns5,$ns6,$ns7,$ns8,$ns9,$ns10];

?>


<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
		
<table class="table table-hover table-dark">
  <table class="table"  border="2" width="100%">
  <thead class="thead-inverse">
    <tr>
      <th scope="col">#</th>
      <th scope="col">NIM</th>
      <th scope="col">UTS</th>
      <th scope="col">UAS</th>
      <th scope="col">Tugas</th>
      <th scope="col">Nilai Akhir</th>
    </tr>
  </thead>



		<?php

			$nomor = 1;
			foreach ($ar_nilai as $ns) {
				echo '<tr><td>'.$nomor;
				echo '<td>' .$ns['nim'].'</td>';
				echo '<td>' .$ns['uts'].'</td>';
				echo '<td>' .$ns['uas'].'</td>';
				echo '<td>' .$ns['tugas'].'</td>';
				$nilai_akhir = ($ns['uts'] + $ns['uas'] +$ns['tugas'])/3;
				echo '<td>' .number_format($nilai_akhir,2,',','.').'</td>';
				echo '<tr/>';
				$nomor++;

			}

		?>		
	</tbody>

</table>




