
<head>
<link href="bootstrap.min.css" rel="stylesheet">
</head>

<body>

<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Form Nilai Siswa</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Nama">Nama Lengkap</label>
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Nama Lengkap" class="form-control input-md">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="matkul">Mata Kuliah</label>
  <div class="col-md-4">
    <select id="matakuliah" name="matakuliah" class="form-control">
      <option value="1">Dasar - Dasar Pemrograman</option>
      <option value="2">Pemrograman Web</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="uts">Nilai Uts</label>
  <div class="col-md-4">
  <input id="UTS" name="UTS" type="text" placeholder="NIlai Uts" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Uas">NIlai Uas</label>
  <div class="col-md-4">
  <input id="UAS" name="UAS" type="text" placeholder="NIlai Uas" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tugas">Nilai Tugas/Praktikum</label>
  <div class="col-md-4">
  <input id="tugas" name="tugas" type="text" placeholder="Nilai Tugas" class="form-control input-md" required="">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button"></label>
  <div class="col-md-4">
   <button id="button" name="button" class="btn btn-primary">Simpan</button>
  </div>
</div>

</fieldset>
</form>

<?php
//definisikan variable
  $proses =$_GET['simpan'];
  $nama =$_GET['nama'];
  $matkul =$_GET['matakuliah'];
  $UTS =$_GET['UTS'];
  $UAS =$_GET['UAS'];
  $tugas =$_GET['tugas'];


  //tampilkan
  echo 'Proses : '.$proses;
  echo '<br/>Nama : '.$nama;
  echo '<br/>Mata Kuliah : '.$matakuliah;
  echo '<br/>Nilai UTS : '.$UTS;
  echo '<br/>Nilai UAS : '.$UAS;
  echo '<br/>Nilai Tugas Praktikum : '.$tugas;
?>
</body>
